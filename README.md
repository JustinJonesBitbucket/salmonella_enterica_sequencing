This program consists of 3 programs to compute the skew diagram, make a subwindow centered around the skew minimum, then reads the subwindow
genome searching for most frequent words that includes patterns within a Hamming Distance D, and the patterns reverse compliments within
Hamming Distance D. This program is implemented on the Salmonella Enterica Genome.

Steps:
-Find the skew minimums for the replication origin:
Run calculate_skew_diagram.py on a genome to find the minimums. Replication of a reverse half-strand proceeds quickly, it lives
double-stranded for most of its life. Conversely, a forward half-strand spends a much larger amount of its life single-stranded,
waiting to be used as a template for replication. This discrepancy between the forward and reverse half-strands is important because
single-stranded DNA has a much higher mutation rate than double-stranded DNA. In particular,
if one of the four nucleotides in single-stranded DNA has a greater tendency than other nucleotides to mutate in single-stranded DNA,
then we should observe a shortage of this nucleotide on the forward half-strand. t turns out that we observe these discrepancies
because cytosine (C) has a tendency to mutate into thymine (T) through a process called deamination.
Deamination rates rise 100-fold when DNA is single-stranded, which leads to a decrease in cytosine
on the forward half-strand. Also, since C-G base pairs eventually change into T-A base pairs, deamination results in the
observed decrease in guanine (G) on the reverse half-strand (recall that a forward parent half-strand synthesizes a reverse daughter
half-strand, and vice-versa). This program finds the greatest minimums for the difference between guanine and cytosine (G-C) as this indicates
the possible location of the replication origin.

-Create the subwindow around the skew minimums. This program creates a slice of the genome into a smaller window to run program mataches.
Run genome_skew_window_creator.py

-Run the program over the subwindow genome to find most common words that also includes mismatches and reverse compliments. This program allows you
to enter length of a polymerase to search for, then most common words of that length (with mismatches for mutations), and reverse compliments
(with mismatches for mutations) are returned.

