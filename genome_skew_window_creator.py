### run skew first to get the minimum G-C from skew diagram for possible origin of replication
#Minimum Locations: Salmonella enterica
#[3764856, 3764858]

fname = "Salmonella_enterica_modified.txt"
with open(fname) as f:
    contentList = f.read().splitlines()
    content = ''.join(contentList)
f.close()

box =500
minimum_location = 3764856
start = minimum_location - box
end = minimum_location + box

subgenome = content[start:end]
#print subgenome
print len(subgenome)
writeFile=open('genome_subwindow.txt', 'w')
writeFile.write(subgenome)
writeFile.close()
print "Job Complete"

