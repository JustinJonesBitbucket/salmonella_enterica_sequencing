import numpy as np

from itertools import izip



def numberToSymbol(symbol):
    value='A'
    if symbol==0:
        value='A'
    elif symbol==1:
        value='C'
    elif symbol==2:
        value='G'
    elif symbol==3:
        value='T'
    return value


def numberToPattern(index, k):
    if k==1:
        return numberToSymbol(index)
    prefixIndex = index / 4 ### this does integer division returning just the quotient
    remainder = index % 4 ### get just the remainder
    symbol = numberToSymbol(remainder)
    prefixPattern = numberToPattern(prefixIndex, k-1)
    return prefixPattern + symbol

def symbolToNumber(symbol):
    value=0
    if symbol == 'A':
        value=0
    elif symbol=='C':
        value=1
    elif symbol=='G':
        value=2
    elif symbol=='T':
        value=3
    #print value
    return value



def patternToNumber(pattern):
    if len(pattern)==0:
        return 0
    # get the last symbol
    symbol = pattern[-1:]
    #print symbol

    prefix = pattern[:-1]
    #print prefix
    return 4 * patternToNumber(prefix) + symbolToNumber(symbol)

def hammingDistance(str1, str2):

    """hamming1(str1, str2): Hamming distance. Count the number of differences

    between equal length strings str1 and str2."""

    # Do not use Psyco

    assert len(str1) == len(str2)

    return sum(c1 != c2 for c1, c2 in izip(str1, str2))

def approximatePatternCount(text, pattern, d):
    count=0
    for i in range (0, len(text) - len(pattern) +1):
        subPattern = text[i:i+len(pattern)]
        if hammingDistance(pattern, subPattern) <= d:
            count+=1
    return count

def firstSymbol(pattern):
    return pattern[0]


def neighbors(pattern, d):
    if d==0:
        #print 'here'
        listToReturn=[]
        listToReturn.append(pattern)
        return listToReturn
    if len(pattern)==1:
        return ['A','C','G','T']
    neighborhood = []
    nucleotides=['A','C','G','T']
    suffixNeighbors = neighbors(pattern[1:], d)
    for text in suffixNeighbors:
        if hammingDistance(pattern[1:], text) < d:
            for element in nucleotides:
                neighborhood.append(element+text)
        else:
            neighborhood.append(firstSymbol(pattern) + text)
    return sorted(neighborhood)

def reverseCompliment(theString):
    #theString = "ACACAC"
    theDict = {'A':'T', 'T':'A', 'G':'C','C':'G'}
    newString=''
    for char in theString:
        newString=newString+(theDict[char])
    ##### now reverse it extended slice syntax.
    ##It works by doing [begin:end:step] - by leaving begin and end off and specifying a step of -1, it reverses a string.
    newString=newString[::-1]
    #print "Original string:", theString
    #print "New String : ", newString
    return newString

def frequentWordsWithMismatchesReverseCompliment(text, k , d):
    ## initialize the frequent Pattern array to return
    frequentPatterns = []
    ##create 2 arrays and initialize to zero
    close = np.zeros((4**k,), dtype=np.int)
    frequencyArray = np.zeros((4**k,), dtype=np.int)
    for i in range(0, len(text)-k+1):     ##Go through the text until k mer from end
        neighborhood = neighbors(text[i:i+k], d) ## find neighbors that differ at the most d in hamming distance from pattern
        for pattern in neighborhood: ## go through each pattern
            #print pattern
            index = patternToNumber(pattern) ## get it's lexigraphical index and set to 1 in the close array
            #print 'index :', index
            close[index]=1
            index2 = patternToNumber(reverseCompliment(pattern))
            close[index2]=1
            #print "close:", close
    for i in range(0, 4**k): ## go through the close array
        if close[i]==1:
            pattern = numberToPattern(i, k) ## if it's positive, set the frequency Array index that corresponds to pattern to number of approx counts
            #print "pattern is", pattern

            frequencyArray[i] = approximatePatternCount (text, pattern, d) + approximatePatternCount(text, reverseCompliment(pattern), d)
    #print frequencyArray
    maxCount = max(frequencyArray) ## find the maximum pattern in the freqarray
    for i in range(0, 4**k): # go through the frequent array, append the max patterns to the frequent pattern array for return
        if frequencyArray[i] == maxCount:
            pattern=numberToPattern(i, k)
            frequentPatterns.append(pattern)
    return sorted(frequentPatterns)

####
### run this for usual datasets
#fname = 'dataset_frequent_words_mismatch_reverse_compliment.txt'
##run this for genomes
fname ='genome_subwindow.txt'
with open(fname) as f:
    contentList = f.read().splitlines()
    content = ''.join(contentList)
f.close()
print "Content Loaded from file"
k=int(raw_input('Please enter a kmer number length: '))
d=int(raw_input('Please enter a max number if mismatches: '))
print "Executing..."
frequentWordsMismatch = frequentWordsWithMismatchesReverseCompliment(content, k,d)

print frequentWordsMismatch
print "Number of frequent words mismatch is :", len(frequentWordsMismatch)
writeFile = open('frequent_words_mismatch_reverse_compliment_output.txt','w')
for element in frequentWordsMismatch:
    writeFile.write(element+' ')
writeFile.close()
print "Job Complete"