def calculateSkew(genome):
    skew=0
    skewList=[]
    skewList.append(skew)
    for char in genome:
        if char=='C':
            skew=skew-1
        elif char=='G':
            skew=skew+1
        skewList.append(skew)
    return skewList


def calculateMinSkewLocations(skewList): ##returns the indices of the minimum locations
   #indices = [i for i, x in enumerate(skewList) if x == min(skewList)]    # Indices of all min occurrences
   indices=[]
   minimum=min(skewList)
   lengthSkew = len(skewList)
   for i in range(0, lengthSkew):
       if skewList[i]==minimum:
           indices.append(i)
   return indices






fname = 'Salmonella_enterica_modified.txt'
with open(fname) as f:
    contentList = f.read().splitlines()
    content = ''.join(contentList)
f.close()
skewList = calculateSkew(content)
#print skewList
minimumSkewLocations = calculateMinSkewLocations(skewList)
print "Minimum Locations: "
print minimumSkewLocations

#writeFile=open('skewList.txt', 'w')
#for element in skewList:
#    writeFile.write(str(element)+' ')
#writeFile.close()

writeFile2=open('skewMinimums.txt', 'w')
for element in minimumSkewLocations:
    writeFile2.write(str(element)+' ')
writeFile2.close()

### Code to plot the graph ... untested
#import matplotlib.pyplot as plt
#plt.plot(skewList)
#plt.ylabel('Genome Skew')
#plt.show()
print 'Job Complete'
